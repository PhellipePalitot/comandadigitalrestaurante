"""System module."""
import os


def limpar_tela():
    """A dummy docstring."""
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")


class ComandaView:
    """System module."""

    @staticmethod
    def exibir_menu_login():
        """A dummy docstring."""
        limpar_tela()
        print("====================== SYNBA-LANCHE ======================")
        print("====================== BEM - VINDO =======================")
        print("================ DIGITE OS DADOS DE LOGIN ================")
        print("==========================================================")

    @staticmethod
    def exibir_menu_garcom():
        """A dummy docstring."""
        limpar_tela()
        print("====================== SYNBA-LANCHE ======================")
        print("===================== TELA - GARCON ======================")
        print("==========================================================")
        print("1 =================== MENU DE PRATOS =====================")
        print("2 ================= FECHAR CONTA DA MESA===================")
        print("3 ===================VISUALIZAR PEDIDOS===================")
        print("==========================================================")

    @staticmethod
    def exibir_menu_cozinheiro():
        """A dummy docstring."""
        limpar_tela()
        print("====================== SYNBA-LANCHE ======================")
        print("==================== TELA - COZINHEIRO ===================")
        print("==========================================================")
        print("1 ==================== LISTAR PEDIDOS ====================")
        print("==========================================================")

    @staticmethod
    def exibir_menu_gerente():
        """A dummy docstring."""
        limpar_tela()
        print("====================== SYNBA-LANCHE ======================")
        print("====================== TELA - GERENTE ====================")
        print("==========================================================")
        print("1 =================== ABRIR RESTAURANTE ==================")
        print("2 ================== FECHAR RESTAURANTE===================")
        print("3 ================ TOTAL DE VENDAS FECHADAS===============")
        print("4 ================ TOTAL DE VENDAS ABERTAS================")
        print("==========================================================")

    @staticmethod
    def exibir_menu_consumiveis(menu):
        """A dummy docstring."""
        print("====================== SYNBA-LANCHE ======================")
        print("===================== MENU DE PRATOS =====================")
        for index in menu:
            string = "Prato: " + index["insumo"] + " | Preço: "
            print(string + str(index["preço-unitário"]) + " reais")
        print("==========================================================")
