"""System module."""
from comanda_controller import ComandaController

APP = ComandaController()


while True:
    CADASTRO = APP.abrir_restaurante()

    if CADASTRO["função"] == "garçom":
        APP.trabalhar_garcom()
    if CADASTRO["função"] == "cozinheiro":
        APP.trabalhar_cozinheiro()
    if CADASTRO["função"] == "gerente":
        APP.trabalhar_gerente()
