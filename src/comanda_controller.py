"""System module."""
import os
import json
from comanda_view import ComandaView, limpar_tela
from comanda_model import Pedido, Garcom, Cozinheiro, Gerente


class ComandaController:
    """System module."""

    def __init__(self) -> None:
        self.__pedidos = []
        self.__menu = {}
        self.__cadastros = {}
        self.__garcom = Garcom()
        self.__cozinheiro = Cozinheiro()
        self.__gerente = Gerente()
        self.__estado_restaurante = False

    def ler_dados_privados(self):
        """A dummy docstring."""
        menu_path = os.path.join(os.getcwd(), "private/menu.json")
        cadastro_path = os.path.join(os.getcwd(), "private/cadastros.json")
        with open(menu_path, encoding="utf-8") as arquivo:
            self.__menu = json.load(arquivo)["menu"]
        with open(cadastro_path, encoding="utf-8") as arquivo:
            self.__cadastros = json.load(arquivo)["cadastros"]

    def fazer_login(self, username, senha):
        """A dummy docstring."""
        for cadastro in self.__cadastros:
            print(cadastro["username"])
            if cadastro["username"] == username and cadastro["senha"] == senha:
                return cadastro
        return ""

    def abrir_restaurante(self):
        """A dummy docstring."""
        self.ler_dados_privados()
        while True:
            ComandaView.exibir_menu_login()
            username = input("Nome de usuário: ")
            senha = input("Senha: ")
            cadastro = self.fazer_login(username, senha)
            if cadastro == "":
                limpar_tela()
                input("DADOS INCORRETOS!")
            else:
                return cadastro

    def trabalhar_garcom(self):
        """A dummy docstring."""
        if self.__estado_restaurante:
            while True:
                ComandaView.exibir_menu_garcom()
                opcao = input("Opção: ")
                limpar_tela()
                if opcao == "1":
                    ComandaView.exibir_menu_consumiveis(self.__menu)
                    escolha = input("Escolha o prato: ")
                    preco = self.checar_pedidos(escolha)
                    if preco:
                        quantidade = input("Quantidade: ")
                        id_mesa = input("Informe a Mesa: ")
                        pedido = Pedido(escolha, int(quantidade), float(preco), id_mesa)
                        self.__garcom.adicionar_pedido(pedido, self.__pedidos)
                        print("Pedido realizado com sucesso!")
                        input("Pressione Enter para continuar...")
                    else:
                        print("Prato escolhido não está no menu: " + escolha)
                if opcao == "2":
                    mesa = input("Qual mesa desejaria fechar? : ")
                    if self.__garcom.checa_estado_mesa(mesa, self.__pedidos):
                        string = "Total da mesa:"
                        print(
                            string
                            + str(
                                self.__garcom.calcula_total_mesa(mesa, self.__pedidos)
                            )
                        )
                        self.__garcom.print_comanda_mesa(mesa, self.__pedidos)
                        confirmacao = input("Deseja finalizar a mesa? (S/N) ")
                        if confirmacao == "S":
                            self.__garcom.finalizar_mesa(mesa, self.__pedidos)
                            input("Mesa finalizada com sucesso!")
                    else:
                        print("A mesa ainda está esperando pratos!!")
                        input("Pressione Enter para continuar...")

                if opcao == "3":
                    mesa = input("Qual mesa desejaria visualizar? : ")
                    limpar_tela()
                    self.__garcom.print_comanda_mesa(mesa, self.__pedidos)
                    input("Pressione Enter para continuar...")
                if opcao == "500":
                    break
        else:
            limpar_tela()
            print("Restaurante fechado!")
            input("Pressione Enter para continuar...")

    def trabalhar_cozinheiro(self):
        """A dummy docstring."""
        if self.__estado_restaurante:
            while True:
                ComandaView.exibir_menu_cozinheiro()
                opcao = input("Opção: ")
                if opcao == "1":
                    estado_entrega = 80
                    limpar_tela()
                    index = self.__cozinheiro.printa_pedidos(self.__pedidos)
                    if index != 0:
                        entregar = input("Deseja entregar algum pedido? (S/N) ")
                        if entregar == "S":
                            prato = input("Qual pedido desejaria entregar? : ")
                            prato = self.__pedidos[int(prato)]
                            estado_entrega = self.__cozinheiro.entregar_pedido(prato)
                    else:
                        print("Não há pedidos para entregar!")
                        input("Pressione Enter para continuar...")
                    if estado_entrega == 200:
                        print("Pedido entregue com sucesso!")
                        input("Pressione Enter para continuar...")
                    elif estado_entrega == 500:
                        print("Pedido inválido!")
                        input("Pressione Enter para continuar...")
                if opcao == "500":
                    break
        else:
            limpar_tela()
            print("Restaurante fechado!")
            input("Pressione Enter para continuar...")

    def trabalhar_gerente(self):
        """A dummy docstring."""
        while True:
            ComandaView.exibir_menu_gerente()
            opcao = input("Opção: ")
            if opcao == "1":
                self.__estado_restaurante = True
                print("Restaurante aberto!")
                input("Pressione Enter para continuar...")
            if opcao == "2":
                self.__estado_restaurante = False
                self.__pedidos = []
                print("Restaurante fechado!")
                input("Pressione Enter para continuar...")
            if opcao == "3":
                string = "VALOR TOTAL DE PEDIDOS FINALIZADOS: R$"
                string = string + str(
                    self.__gerente.total_de_vendas_fechadas(self.__pedidos)
                )
                self.__gerente.printa_comanda_fechada(self.__pedidos)
                print(string)
                input("Pressione Enter para continuar...")
            if opcao == "4":
                string = "VALOR TOTAL DE PEDIDOS EM ANDAMENTO: R$"
                print(string + str(Gerente.total_vendas_abertas(self.__pedidos)))
                self.__gerente.printa_comanda_aberta(self.__pedidos)
                input("Pressione Enter para continuar...")
            if opcao == "500":
                break

    def checar_pedidos(self, escolha):
        """A dummy docstring."""
        for indice in self.__menu:
            if indice["insumo"] == escolha:
                return indice["preço-unitário"]
        return 0

    def calcula_total_mesa(self, id_mesa):
        """A dummy docstring."""
        total = 0
        for indice in self.__pedidos:
            if indice.getMesa() == id_mesa:
                total += indice.get_preco()
        return total
