"""System module."""


class Pedido:
    """A dummy docstring."""

    def __init__(self, insumo: str, qnt: int, preco: float, mesa: int):
        self.__nome_insumo = insumo
        self.__quantidade = qnt
        self.__mesa = mesa
        self.__preco_unitario = preco
        self.__estado = "Pendente"
        self.__finalizado = False

    def set_estado(self, estado):
        """A dummy docstring."""
        self.__estado = estado

    def get_estado(self):
        """A dummy docstring."""
        return self.__estado

    def set_finalizado(self, finalizado):
        """A dummy docstring."""
        self.__finalizado = finalizado

    def get_finalizado(self):
        """A dummy docstring."""
        return self.__finalizado

    def get_mesa(self):
        """A dummy docstring."""
        return self.__mesa

    def get_total(self):
        """A dummy docstring."""
        return self.__quantidade * self.__preco_unitario

    def get_comanda(self):
        """A dummy docstring."""
        string = "=============================="
        string = string + "\n- Prato: " + self.__nome_insumo
        string = string + "\n - Quantidade: " + str(self.__quantidade)
        string = (
            string + "\n - Mesa:  " + str(self.__mesa) + "\n - Estado: " + str(self.__estado)
        )
        string = string + "\n==============================\n"
        return string


class Garcom:
    """System module."""

    def __init__(self):
        pass

    @staticmethod
    def adicionar_pedido(item: Pedido, pedido: list):
        """A dummy docstring."""
        pedido.append(item)

    @staticmethod
    def print_comanda_mesa(mesa: int, pedidos: list):
        """A dummy docstring."""
        for index in pedidos:
            if index.get_mesa() == mesa and not index.get_finalizado():
                print(index.get_comanda())

    @staticmethod
    def calcula_total_mesa(mesa: int, pedidos: list):
        """A dummy docstring."""
        total = 0.0
        for index in pedidos:
            if index.get_mesa() == mesa and not index.get_finalizado():
                total = total + float(index.get_total())
        return total

    @staticmethod
    def finalizar_mesa(mesa: int, pedidos: list):
        """A dummy docstring."""
        for index in pedidos:
            if index.get_mesa() == mesa and not index.get_finalizado():
                index.set_finalizado(True)

    @staticmethod
    def checa_estado_mesa(mesa: int, pedidos: list):
        """A dummy docstring."""
        for index in pedidos:
            if index.get_mesa() == mesa and index.get_estado() == "Pendente":
                return False
        return True


class Cozinheiro:
    """System module."""

    def __init__(self) -> None:
        pass

    @staticmethod
    def entregar_pedido(pedido: Pedido):
        """A dummy docstring."""
        if pedido.get_estado() == "Pendente":
            pedido.set_estado("Entregue")
            return 200
        return 500

    @staticmethod
    def printa_pedidos(pedidos: list):
        """A dummy docstring."""
        lista = 0
        qnt = 0
        for index in pedidos:
            if index.get_estado() == "Pendente":
                print(
                    "Pedido " + str(lista) + " - \n" + str(index.get_comanda()) + "\n"
                )
                qnt = qnt + 1
            lista = lista + 1
        return qnt


class Gerente:
    """System module."""

    def __init__(self):
        pass

    @staticmethod
    def total_de_vendas_fechadas(comandas: list):
        """A dummy docstring."""
        total = 0.0
        for index in comandas:
            if index.get_finalizado():
                total = total + float(index.get_total())
        return total

    @staticmethod
    def total_vendas_abertas(comandas: list):
        """A dummy docstring."""
        total = 0.0
        for index in comandas:
            if not index.get_finalizado():
                total = total + float(index.get_total())
        return total

    @staticmethod
    def printa_comanda_fechada(comandas: list):
        """A dummy docstring."""
        for index in comandas:
            if index.get_finalizado():
                print(index.get_comanda())

    @staticmethod
    def printa_comanda_aberta(comandas: list):
        """A dummy docstring."""
        for index in comandas:
            if not index.get_finalizado():
                print(index.get_comanda())
