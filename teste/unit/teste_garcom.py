"""System module."""
from comanda_model import Garcom, Pedido


def run_garcom_unit_tests():
    """A dummy docstring."""
    return True

def test_checa_estado_mesa():
    """A dummy docstring."""
    garcom = Garcom()
    assert not garcom.checa_estado_mesa(1, [Pedido("pizza", 4, 10, 1)])

def test_finalizar_mesa():
    """A dummy docstring."""
    garcom = Garcom()
    lista = []
    lista.append(Pedido("pizza", 5, 10,1))
    lista.append(Pedido("pudim", 10, 4,1))
    garcom.finalizar_mesa(1, lista)
    assert lista[0].get_finalizado()

def test_calcula_total_mesa():
    """A dummy docstring."""
    garcom = Garcom()
    lista = []
    lista.append(Pedido("pizza", 5, 10,1))
    lista.append(Pedido("pudim", 10, 4,1))
    assert garcom.calcula_total_mesa(1, lista) == 90

def test_print_comanda_mesa():
    """A dummy docstring."""
    garcom = Garcom()
    lista = []
    lista.append(Pedido("pizza", 5, 10,1))
    lista.append(Pedido("pudim", 10, 4,1))
    garcom.print_comanda_mesa(1, lista)

def test_adicionar_pedido():
    """A dummy docstring."""
    garcom = Garcom()
    lista = []
    garcom.adicionar_pedido(Pedido("pudim", 10, 4,1), lista)
    assert lista[0]
