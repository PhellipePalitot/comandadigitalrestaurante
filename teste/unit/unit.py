"""System module."""
# pylint: disable=E0611
from .teste_cozinheiro import run_cozinheiro_unit_tests, test_entregar_pedido, test_printa_pedidos
from .teste_garcom import run_garcom_unit_tests, test_checa_estado_mesa, test_adicionar_pedido
from .teste_garcom import test_finalizar_mesa, test_calcula_total_mesa, test_print_comanda_mesa
from .teste_gerente import run_gerente_unit_tests, test_total_de_vendas_fechadas
from .teste_gerente import test_total_vendas_abertas
from .test_view import test_exibir_menu_gerente, test_exibir_menu_cozinheiro
from .test_view import test_exibir_menu_consumiveis, test_exibir_menu_login, test_exibir_menu_garcom
from .test_controller import test_ler_dados_privados

def run_unit_tests():
    """A dummy docstring."""
    run_cozinheiro_unit_tests()
    run_garcom_unit_tests()
    run_gerente_unit_tests()
    test_entregar_pedido()
    test_printa_pedidos()
    test_total_de_vendas_fechadas()
    test_total_vendas_abertas()
    test_checa_estado_mesa()
    test_finalizar_mesa()
    test_calcula_total_mesa()
    test_print_comanda_mesa()
    test_adicionar_pedido()
    test_exibir_menu_login()
    test_exibir_menu_garcom()
    test_exibir_menu_cozinheiro()
    test_exibir_menu_gerente()
    test_exibir_menu_consumiveis()
    test_ler_dados_privados()
    