"""System module."""
import os
import sys

fpath = os.path.join(os.path.dirname(__file__), "../../src/")
sys.path.append(fpath)
# pylint: disable=C0413
from .unit import run_unit_tests

run_unit_tests()
