"""System module."""
# pylint: disable=E0401
from comanda_view import ComandaView

def test_exibir_menu_login():
    """A dummy docstring."""
    ComandaView.exibir_menu_login()

def test_exibir_menu_garcom():
    """A dummy docstring."""
    ComandaView.exibir_menu_garcom()

def test_exibir_menu_consumiveis():
    """A dummy docstring."""
    menu = [
    {
        "insumo": "macarronada",
        "preço-unitário": 32
    }]
    ComandaView.exibir_menu_consumiveis(menu)

def test_exibir_menu_cozinheiro():
    """A dummy docstring."""
    ComandaView.exibir_menu_cozinheiro()

def test_exibir_menu_gerente():
    """A dummy docstring."""
    ComandaView.exibir_menu_gerente()

