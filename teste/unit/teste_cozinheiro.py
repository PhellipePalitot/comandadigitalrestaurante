"""System module."""
from comanda_model import Cozinheiro
from comanda_model import Pedido

def run_cozinheiro_unit_tests():
    """A dummy docstring."""

def test_entregar_pedido():
    """A dummy docstring."""
    cozinheiro = Cozinheiro()
    valor_test = cozinheiro.entregar_pedido(Pedido("pizza", 25, 23.4,5))
    assert valor_test == 200

def test_printa_pedidos():
    """A dummy docstring."""
    cozinheiro = Cozinheiro()
    lista = []
    lista.append(Pedido("pizza", 25, 23.4,5))
    lista.append(Pedido("pudim", 25, 23.4,5))
    test = cozinheiro.printa_pedidos(lista)
    assert test != 0
    