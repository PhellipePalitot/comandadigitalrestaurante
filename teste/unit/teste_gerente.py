"""System module."""
from comanda_model import Gerente, Pedido


def run_gerente_unit_tests():
    """A dummy docstring."""
def test_total_de_vendas_fechadas():
    """A dummy docstring."""
    gerente = Gerente()
    total = gerente.total_de_vendas_fechadas([Pedido("pizza", 3, 10,5)])
    print(total)
    assert total == 0

def test_total_vendas_abertas():
    """A dummy docstring."""
    gerente = Gerente()
    lista = []
    lista.append(Pedido("pizza", 5, 10,5))
    lista.append(Pedido("pudim", 10, 4,5))
    total = gerente.total_vendas_abertas(lista)
    assert total == 90
